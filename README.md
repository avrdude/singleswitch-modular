# SingleSwitch Modular

**SingleSwitch Modular** is the modular keyboard based on Amoeba and inspired by Pallete that is made for creating absolutely modular keyboards. 
Followed by ortholinear design and simplicity this _board_ will make your ideas come true!

Features:
1. Fully open-source
2. Based on Amoeba PCB design
3. Doesn't require expensive parts
4. Compatible with MX style switches (Cherry, Kailh, Gateron, Zeal, etc)
5. Works with QMK software
6. Supports endless amount of buttons
7. Fully customizable and expandable (you can do your own modules)
